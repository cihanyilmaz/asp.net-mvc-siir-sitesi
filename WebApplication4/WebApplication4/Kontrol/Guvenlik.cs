﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Kontrol
{
    public class Guvenlik :System.Web.Mvc.Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(Session["Kullanici"]==null)
            {
                filterContext.Result = new RedirectResult("~/Home/Login");
                return;

            }

            base.OnActionExecuting(filterContext);
        }

    }
}