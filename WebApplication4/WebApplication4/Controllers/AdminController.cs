﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication4.Kontrol;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
   
    public class AdminController : Guvenlik
    {
       
        WebEntities en = new WebEntities();

        public ActionResult Cikis()
        {

            FormsAuthentication.SignOut();
            Session["Kullanici"] = null;

            return RedirectToAction("Index", "Home");
        }


      

        
        public ActionResult Admin()
        {
            if ((string)@Session["Kullanici"] == "c")

            {
                HomeController.AnaSayfaDTO obj = new HomeController.AnaSayfaDTO();

                obj.siirler = en.Siirler.ToList();
                //obj.yorumlar = en.Yorumlar.ToList();
                return View(obj);
            }
            if ((string)@Session["Kullanici"] == null)
                return RedirectToAction("Login", "Home");

            return RedirectToAction("UyePaneli", "Admin");

        }
       
        public ActionResult SiirEkle()
        {
            return View();

        }
       
        [HttpPost]
        public ActionResult YeniSiir(Siirler s)
        {
            //Siirler siir = new Siirler();

            //siir.baslik = s.baslik;
            //siir.siir = s.siir;
            //siir.yazar = s.yazar;
            s.ekleyen = (string)@Session["Kullanici"];

            en.Siirler.Add(s);
            en.SaveChanges();

            //if((string)@Session["Kullanici"]=="c")
            //return RedirectToAction("Admin", "Home");


            return RedirectToAction("UyePaneli", "Admin");


        }
        
        public ActionResult SiirSil(int siirid)
        {
            en.Siirler.Remove(en.Siirler.First(d => d.id == siirid));
            en.SaveChanges();
            //if ((string)@Session["Kullanici"] == "c")
            //    return RedirectToAction("Admin", "Home");

            return RedirectToAction("Admin", "Admin");


        }
        public ActionResult SiirDuzenle(int id)
        {

            Siirler s = en.Siirler.Find(id);
            return View(s);


        }
        
        [HttpPost]
        public ActionResult SiirDuzenle(FormCollection coll)
        {
            //var fdsrr = coll["id"];

            //var dfsdfd = TryUpdateModel(siir);
            //var up = en.Siirler.Find(siir.id);
            int a = Int32.Parse(coll["id"]);

              var up = en.Siirler.Where(x => x.id == a).FirstOrDefault();
            //int.Parse.
                
             up.baslik = coll["baslik"];
            up.siir = coll["siir"];
            up.yazar = coll["yazar"];

            //en.Entry(siir).State = EntityState.Modified;

            en.SaveChanges();

            return RedirectToAction("Admin", "Admin");

        }

       


       
        public ActionResult UyePaneli()
        {
            if ((string)@Session["Kullanici"] == null)
                return RedirectToAction("Login", "Home");

            HomeController.AnaSayfaDTO obj = new HomeController.AnaSayfaDTO();
            String kullanici = (string)@Session["Kullanici"];
            obj.siirler = en.Siirler.Where(x => x.ekleyen == kullanici).ToList();
            //obj.yorumlar = en.Yorumlar.ToList();
            return View(obj);



        }

       
    }
}