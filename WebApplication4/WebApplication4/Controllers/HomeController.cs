﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        WebEntities en = new WebEntities();
        public ActionResult Index()
        {
            AnaSayfaDTO anasayfa = new AnaSayfaDTO();
            anasayfa.siirler = en.Siirler.OrderByDescending(x => x.id).ToList();
            return View(anasayfa);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkında Sayfası";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (@Session["Kullanici"]!=null)
            {

                return RedirectToAction("Index", "Home");
            }
            return View();


        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(Kullanici k)
        {
            var hesap = en.Kullanici.Where(x => x.kullaniciAdi == k.kullaniciAdi && x.sifre == k.sifre).Count();
            if(hesap==0)
            {
                return RedirectToAction("Login", "Home");

            }
            else
            {

        
                if(k.kullaniciAdi=="c")
                {
                    FormsAuthentication.SetAuthCookie(k.kullaniciAdi, true);
                    Session["Kullanici"] = k.kullaniciAdi;


                    return RedirectToAction("Admin", "Admin");


                }
                FormsAuthentication.SetAuthCookie(k.kullaniciAdi,true);
                Session["Kullanici"] = k.kullaniciAdi;


                return RedirectToAction("UyePaneli", "Admin");

            }

        }

       
        public ActionResult KayitOl()
        {
            ViewBag.Message = "Kayıt Sayfası";

            return View();
        }
        [HttpPost]
        public ActionResult YeniUye(Kullanici k)
        {
            Kullanici yeni = new Kullanici();
            yeni.kullaniciAdi = k.kullaniciAdi;
            yeni.adi = k.adi;
            yeni.soyadi = k.soyadi;
            yeni.email = k.email;
            yeni.sifre = k.sifre;
            en.Kullanici.Add(yeni);
            en.SaveChanges();
            return RedirectToAction("Login", "Home");

        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

        public class AnaSayfaDTO
        {
            public List<Siirler> siirler { get; set; }
      
        }
    }
}